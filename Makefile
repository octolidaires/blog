.PHONY: build deploy build-and-deploy

build:
	hugo -d ../blog-solidaires-build/

deploy:
	cd ../blog-solidaires-build
	git add .
	git commit -m "update"
	git push origin master

build-and-deploy: build deploy