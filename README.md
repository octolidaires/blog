# Blog de la section Solidaires Informatiques de OCTO Technology

Pour éditer et publier le site, il faut :
- installer hugo
- savoir utiliser git
- faire un clone du dépôt "blog"

Pour cloner le dépôt :
```
git clone https://codeberg.org/octolidaires/blog.git
```

ou
```
git clone git@codeberg.org:octolidaires/blog.git
```

Pour initialiser le thème (prendre en compte le sous module git) :
```
git submodule init
git submodule update
```

Pour tester le site en direct pendant l'édition, il faut lancer hugo en mode serveur
et accéder au site sur http://localhost:1313/
```
hugo server
```





