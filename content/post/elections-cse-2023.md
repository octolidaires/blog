---
title: "🗳️ ÉLECTIONS CSE 2023: On vous explique tout !"
date: "2023-01-05"
draft: false
author: "Des Octos"
---

Fin janvier, le <abbr title="Comité Social et Économique">CSE</abbr> (Comité Social et Économique) se renouvelle. Les elections des 31 janvier et 14 février vous permettrons, à vous les Octos, de choisir qui vous représentera auprès de la direction. On vous explique tous les enjeux de cette élection, comment fonctionne le <abbr>CSE</abbr> (qui reprends les rôles de la <abbr title="Délégation Unique du Personnel">DUP</abbr>, du <abbr title="Comité d'Entreprise">CE</abbr> et du <abbr title="Comité Hygiène Sécurité et Conditions de Travail">CHSCT</abbr>), et si jamais tout ces acronymes sont trop barbares, on vous les explique aussi dans notre mini-formation.

<!--more-->

## La liste Solidaires a besoin des Octos !

### Les enjeux de la prochaine élection 🗳️

<a style="float:left; display:inline-block; margin-right: 20px; margin-bottom: 20px;" href="/pdf/tract%20appel%20a%20candidatures.pdf">  
<img src="/img/tract%20appel%20a%20candidatures%20mini.png">  
</a>  

**C'est un moment clef pour OCTO** : depuis les dernières élections professionnelles, notre entreprise a presque doublé de taille, notamment en accueillant de nouveaux Octos venant d'Accenture et Benext. Nous vivons un changement massif de nos habitudes, à la fois dans notre manière de travailler et dans ce qu'impliquent nos déménagements partout en France, auquel vient s’ajouter la réorganisation en cours.  

**OCTO a changé et nous le devons aussi.** OCTO n'a toujours pas de budget Activités Sociales et Culturelles (anciennement appelé "<abbr>CE</abbr>" ou Comité d'Entreprise). La question de l'égalité de traitement entre les Octos en campements, en Île-de-France et en remote partout en France est un irritant grandissant. Les inquiétudes quant à la rétention des Octos expérimenté.e.s alimentent nombre de discussions et ne trouvent pas encore de solutions satisfaisantes.  

**Comment faire pour qu’OCTO continue d’être une _Best Place to Grow_ ?**<br/>  
**En venant co-construire avec nous le programme et participer aux discussions ! Plus d'informations sur notre [programme](/programme).**  

### Pourquoi rejoindre une liste ? 📑
**S'engager sur une liste aux élections professionnelles,** c'est porter la voix de ses camarades, les tenir au courant des informations importantes et les représenter dans les négociations avec la direction pour améliorer continuellement le quotidien des Octos.  

**Pour qu’OCTO reste une boîte où il fait bon vivre,** nous avons besoin de représentant•e•s du personnel qui pourront s’impliquer dans la vie démocratique de l’entreprise, pour remonter des alertes, porter des projets, s'assurer du respect de la loi, des droits des salarié•e•s, de la sécurité et des intérêts de chacun•e.  

### Est-ce que je dois adhérer à un syndicat ? 🚩
**Non** : La liste est **soutenue** par le syndicat Solidaires-Informatique afin de pouvoir se présenter dès le premier tour. Mais les candidat•e•s n'ont pas besoin d'être adhérent•e•s.  

### Pourquoi Solidaires-Informatique ? 👋
**Depuis 4 ans, au sein d'OCTO,** Solidaires agit au cœur du <abbr>CSE</abbr>, malgré sa position minoritaire. Nous avons remonté vos revendications, partagé les informations importantes, porté le sujet de l'indemnité kilométrique vélo, soutenu les salarié•e•s d'Appaloosa et OCTO PACA lors des fermetures de ces entités, alerté sur les conditions d'arrivée des Benexters…  

**[Le syndicat Solidaires](/syndicat)** a un projet de transformation sociale qui repose sur un certain nombre de principes: l’intérêt général, la mise en commun des ressources, le partage équitable des richesses, l’égalité des droits ou encore le respect des libertés fondamentales et la préservation de l’environnement.  

**Envie d'en discuter ou de nous rejoindre ? Ça se passe sur [contact](/contact).**  

[Retrouvez le tract complet en PDF.](/pdf/tract%20appel%20a%20candidatures.pdf)  
## Vous ne comprenez rien au CSE, Délégué Syndicaux, CE, Liste ?
On vous explique tout sur [ce deck de slides](/pdf/slides_cse.pdf).  
