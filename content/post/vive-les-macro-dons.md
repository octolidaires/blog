---
title: "Vivent les macro-dons ! 💰"
date: "2022-06-23"
draft: false
author: "Des Octos"
---

Nous sommes interpellés par les incitations régulières aux micro-dons proposés par la direction d'OCTO aux employé·e·s.  

Nous tenons à rappeler à la direction que tout notre argent provient de notre employeur: c'est pourquoi, dans un esprit "*lean*", nous lui proposons de ne pas passer par les employé·e·s et l'encourageons à réaliser ces dons à des associations directement !  

La plupart de ces associations n'existent que parce que le service public n'est pas à la hauteur, la faute entre autres à l'évasion fiscale généralisée des entreprises qui grève le budget de l’État.  
Nous proposons donc à Accenture de participer à l'éffort collectif et de rapatrier toutes ses entreprises, holdings et filiales en France, pays qui sait redistribuer les richesses dans le service public.  

Par aillieurs, au vue de l'inflation, il nous semble un peu dérangant de voir la direction nous demander de diminuer nos salaires.  

Enfin, si vraiment OCTO ou Accenture trouvent que leurs poches sont trop pleines, nous pouvons leur fournir les noms d'une multitude d'associations, de collectifs ou de caisses de grèves qu'ils pourraient financer !  
