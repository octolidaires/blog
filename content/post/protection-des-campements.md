---
title: "Pour une meilleure protection des Octos en campements 🏕️"
date: "2023-01-01"
draft: false
author: "Des Octos"
---

Fin septembre, décision a été prise par la direction de fermer le campement PACA, mais plutôt que de vous expliquer le contexte nous-même, nous laissons les Octos concerné•e•s s'exprimer avec le texte suivant :

<blockquote>
“L’aventure entrepreneuriale du campement PACA qui a duré 4 ans, aura développé le business OCTO en Provence avec fierté et renforcé les rangs jusqu'à atteindre 14 collaborateurs.

Celle-ci a pris fin, suite à une décision unilatérale et brutale, sans concertation, sans exploration d'alternatives, sans accompagnement, pourtant annoncé, des ex-campeurs. Aux démissions des dirigeants, se sont par conséquent ajoutés les départs de collaborateurs de grande valeur, portant le total à 6 démissions.

Il aura fallu que les collaborateurs restants – qui subissent de plein fouet cette décision mais veulent pleinement continuer à évoluer au sein d’OCTO – se rapprochent des représentants syndicaux pour que leurs conditions de travail, ainsi chamboulées, puissent être abordées.

La nouvelle a été annoncée le 22 septembre, et depuis cette date, aucune décision n’a été prise, hormis celle de leur rattachement immédiat à Paris. Ceci affecte fortement leur moral et génère chez eux beaucoup d’inquiétude.

La seule avancée obtenue après 2 mois de flou consiste en une évolution de l'expérimentation (provisoire) OctoFromAnywhere avec la prise en charge des déplacements pour 2j par mois au lieu d'un seul.

Il est plus que jamais urgent de faire valoir l’humain tant prôné chez OCTO et de garantir leurs conditions de rattachement à Paris, notamment : frais de déplacement, réévaluation de leurs salaires "provinciaux" et équilibre de vie. Bref, simplement leur accorder toute la considération qu’ils méritent dans ce changement subit et ne pas juste les déplacer comme des pions.

Les représentants syndicaux envisagent déjà d’activer des recours juridiques pour faire avancer leurs demandes si aucun avancement significatif n’était trouvé dans leur dialogue avec la Direction.”
</blockquote>

Cette situation nous paraît en effet déplorable : les Octos concerné•e•s se retrouvent face à un mur, la Direction ne souhaitant pas spécialement négocier de conditions, se rangeant derrière des clauses du contrat de travail pour le moins larges et peu solides juridiquement. 

Nous ne pouvons également que dénoncer les pressions qui ont été exercées sur certain•e•s Octos pour les pousser à demander des ruptures conventionnelles, ou à faire traîner la situation dans le flou, poussant certain•e•s à poser leurs démissions en l’absence de plan d’action clair et satisfaisant pour leur avenir. 
Ce procédé avait déjà été utilisé lors de la fermeture d’Appaloosa, sujet sur lequel d’ailleurs certains avaient obtenu de vraies compensations avec l’aide de notre organisation syndicale. Cette situation précaire est génératrice d’anxiété, de stress pour les Octos, et endommage les relations Octos/Direction. Cette situation ne peut perdurer.

L'absence de structure juridique du campement rend également les salarié•e•s très vulnérables aux désidératas de la direction. En effet, bien qu'il soit établi que les Octos Provence habitent et travaillent principalement à Marseille et autour, ils et elles sont contractuellement considéré•e•s comme pouvant travailler à Marseille ET en Île de France.

Nous ne voulons pas qu’une telle situation puisse se reproduire pour les autres campements. Il existe aujourd’hui deux situation totalement différentes, qui ne peuvent et ne doivent être assimilées l’une à l’autre : 
* L'expérimentation *OCTO From Anywhere*, choisie, où certain•e•s Octos sont partis habiter partout en France et en ont accepté dès le début les contraintes allant de pair avec leur choix de vie. 
* La situation d’Octos ayant toujours habité dans une région, rejoint une entreprise qui a des bureaux dans cette région, et qui se retrouvent désormais contraint•e et forcé•e d’accepter une situation intenable, tant financièrement qu’en termes de déplacements.

Avec les bonnes structures juridiques, la fermeture d'OCTO Provence (qui rappelons-le, est motivée par des raisons économiques car OCTO Provence, citons la plénière *"n'a pas trouvé son Product Market Fit"* !) : 
* Aurait amené à des licenciements et les compensations liées, plutôt que des démissions et des ruptures conventionnelles subies
* À de vraies propositions de requalification, comprenant des packages de relocalisation si nécessaire
* Aurait permis (et permettrait toujours) de considérer les missions loin du lieu de travail habituel et reconnu de l'Octo dans son contrat de travail comme des déplacements professionnels (et donc de prendre en charge l’intégralité des frais afférents et des primes d'éloignement si nécessaire)
* Permettrait une meilleure représentation des Octos concerné•e•s avec des délégués du personnel de proximité, etc.

Utiliser le statut juridique d’établissement secondaire est un vrai moyen de mettre au clair une situation parfois floue, que ce soit dans les faits ou dans le contrat de travail, et permet d’éviter les situations précaires. **C'est pourquoi nous demandons à ce que les campements soient déclarés comme des établissements secondaires d'OCTO** (comme l'est le Vaisseau actuellement), afin que les salarié•e•s concerné•e•s puissent bénéficier de ces clarifications.

Nous continuons à penser que la situation en PACA aurait pu être mieux préparée et les salarié•e•s mieux accompagné•e•s par la direction et les ressources humaines afin de rendre tout changement le plus simple possible. Nous invitons la direction à se reporter aux textes légaux de référence concernant les déplacements et mutations de ses salarié•e•s.

