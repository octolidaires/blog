---
title: "La liste Solidaires rédige son mail de campagne avec 🤖ChatGPT. Ça tourne mal ! 😱"
date: "2023-01-04"
draft: false
author: "Des Octos"
---

Rappelle-toi du monde d’il y a 4 ans. Pas de covid, pas de guerre en Ukraine, Hulot venait de démissionner, OCTO comptait 600 salarié·e·s… 4 ans c’est la durée du mandat des prochain·e·s représentant·e·s du personnel. 
Tout peut changer en 4 ans ; Alors même si aujourd’hui tu te dis *“Tout va bien à OCTO pas besoin de représentant·e·s”*, peut-être que les quelques minutes de ton temps, investi à lire cet email, et aussi à voter pour la liste Solidaires **le 31 janvier prochain**, sont précieuses. Pour toi, pour tes collègues, pour les prochain·e·s Octos, et au-delà, tu ne pourras pas dire que tu étais seul·e !

<small>(Au fait, c'est faux : ce mail est rédigé par de vraies personnes.)</small>

## 🔥 TL;DR

<center>
    <img src="/img/meme_campagne.webp" alt="Le mème Drake no / Drake yes, avec dans la case Drake no 'Ne pas lire ce mail' et dans la case Drake yes 'Voter pour la liste Solidaires'" />
</center>

Nos grandes propositions pour le prochain mandat : 

**1 - Renforcer et professionnaliser le CE :** le saviez-vous ? **OCTO ne respecte pas [la loi](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000036761976)** à propos du budget du CE qui doit être a minima constant vis-à-vis de la masse salariale, or, **ce budget a disparu** depuis 2019. Concrètement, cela rend aujourd'hui impossible un vrai CE avec des avantages pour les Octos et leurs familles, ainsi que [notre souhait d'embaucher](http://solidaires-octo.com/programme/#ce) une personne dédiée à sa gestion. Rendez l'argent !

**2 - Libérer l’information :** OCTO a une obligation légale de mettre à disposition du CSE une BDESE (Base de Données Économiques, Sociales et Environnementales). Aujourd’hui cette base n’existe pas. Vos représentant•e•s n’ont pas les informations utiles pour agir, ni pour vous informer.

**3 - Les salarié•e•s en charge du WHY :** incarner un rapport de force face à la direction pour mettre en actes, sans greenwashing, la raison d’être d’OCTO au bénéfice de toutes et tous. Nous souhaitons que les salarié•e•s, par leurs représentant•e•s au CSE, soient des acteurs et actrices engagé•e•s de cette nouvelle trajectoire, parce que c'est notre WHY !

**4 - Obtenir le remboursement à 100% des abonnements de transports en commun !** Le transport des salarié·e·s est l’un des leviers majeurs de décarbonation pour OCTO et ne pas avoir une action forte sur les mobilités douces ne nous aide pas à atteindre les objectifs que nous nous sommes fixés collectivement.

<p><center>
<a href="https://solidaires-octo.com/programme">RETROUVEZ SUR LE BLOG L'INTÉGRALITÉ DE NOTRE PROGRAMME !</a>
</center></p>

Mobilité, outillage et télétravail, discriminations, bien être et santé au travail, etc. Tout y passe.

## 🐥 Des questions ? 
Des points pas clairs ? Besoin d’éclaircissement ? Pose tes questions directement sur [cette spreadsheet](https://docs.google.com/spreadsheets/d/1Yy4cp-qsQksWx1cfdLxomTTA1Hza2uK22lUcYyXrHl4/edit#gid=0), on y répondra dans notre prochain email et sur le blog.

Tu peux aussi pinguer directement les candidat•e•s points de contacts : TLU, LAAL, SEBA, LIBO, JORO, EST & YART.


## 🎁 Un peu de temps pour de la lecture ?

*La liste Solidaires met à disposition du contenu engagé et éducatif sur les sujets qui concernent les Octos !*

* **Le CSE qu'est ce que c'est ? :** [En bon consultants, on a fait des slides pour tout expliquer !](/pdf/slides_cse.pdf)<br />
    Le Comité Social et Économique a pour rôle la représentation des Octos auprès de la direction, notamment lors de réunions mensuelles. 
    Son rôle est notamment de :
    * S’assurer du bon respect de la loi, des conditions de travail et de la sécurité des salarié•e•s
    * S’assurer de la transparence de la direction sur les décisions prises et faire le lien remontant des salarié•e•s
    * Assurer le bon fonctionnement des Activités Sociales et Culturelles (feu CE: Comité d'Entreprise)

* **Pour une meilleure protection des campements**<br />
La fermeture du campement Provence nous interroge. Sans concertation, sans protection, les Octos de (feu) PACA, rattachés désormais à OCTO Paris, se retrouvent en situation précaire quant à la prise en charge de leurs frais de déplacement à Paris et de leur qualité de vie (voire de leur emploi). <br />
Cette situation aurait pu être évitée avec les bonnes structures légales et avec une gestion de cette transition plus humaine, et profiter à tous les campements. Cela nécessite des compétences juridiques que la plupart des Octos n'ont pas, mais dont Solidaires dispose.  
Pourquoi nous proposons de créer des “établissements” pour chaque campement ? Retrouvez tout dans [cet article](/post/protection-des-campements).

* **De l'utilité de se faire entendre** <br />
Grâce à celles et ceux qui ont un peu élevé la voix un peu partout, le fameux *“95% ferme de TACE pour les N1”* est devenu un *“95% MAIS…”* (avec beaucoup de marge de manœuvre). <br />
Preuve s'il en faut que même les décisions les plus unilatérales peuvent être remises en cause. C'est la force du collectif, et tout l'intérêt d'avoir des représentants et des syndicats pour porter la voix des Octos au plus près de la direction.

* **Un nouveau why qui nous oblige** <br />
Au sein de la liste Solidaires, on adore le nouveau WHY d'OCTO. Cela étant dit, nous ne devons pas le laisser être simplement des paroles en l'air : il porte des convictions fortes sur la sobriété, la nécessité de créer de nouvelles façons de produire et de protéger notre environnement. <br />
Ces revendications sont au cœur du programme de Solidaires depuis plus de 10 ans. C’est pourquoi nous nous engageons à être au quotidien le poil à gratter qui fera de notre WHY un véritable projet de transformation de notre entreprise ! <br />
Voir [le cahier revendicatif de Solidaires](https://solidaires.org/se-syndiquer/nos-positions/)

* **Et encore plus dans notre programme** <br />
On vous a dit qu'on avait un vrai programme pour le CSE ? Qu’il y avait plein d’autres sujets et beaucoup de détails ? Qu'il était disponible [en cliquant là ? Juste ici oui](/programme).

**PS:** ✊ Manif’ pour les retraites le **samedi 21 janvier** ! Pas envie de faire de l’Angular ou de plonger dans les arcanes de JIRA à 65 ans ? Pourquoi on est contre la réforme et toutes les informations pratiques sur [notre article dédié](/post/greve-reforme-retraites).