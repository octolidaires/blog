---
title: "Des grèves pour sauver nos retraites 👊"
date: "2022-12-01"
draft: false
author: "Des Octos"
summary: "Le gouvernement veut mettre en place un départ à la retraite à 64 ou 65 ans 😠  

À votre avis, dans 30 ans, votre expertise en node.js, elle vaudra quoi ? Votre certification SCRUM ? Votre expertise Azure ?  

On le voit déjà, tous les développeuses et développeurs experts de techno dépassées (delphi, pascal, lisp…) galèrent à trouver du travail passé 50 ans, alors que dans le même temps les jeunes peinent à entrer sur le marché du travail."
---

On vous rappele que le **21 Janvier** a lieu une première manif contre la nouvelle réforme des retraites. Vu que c'est un samedi, ce n'est pas une grève, mais on peut être certain qu'il va y en avoir très prochainement qui vont tomber ! N'hésites pas à pinger [les membres de la liste](/contact) pour se rejoindre en manif, si tu as des questions technique ou pour discuter ! <br />
Bref, des mobilisations (manifs et grèves) s'organisent. Restez informé, nous on sera là !<br />
(PS : Pour [faire grève](https://greve.cool), dans Octopod tu mets juste *"congé sans solde"* et voila.)

----

Le gouvernement veut mettre en place un départ à la retraite à 64 ou 65 ans 😠  

À votre avis, dans 30 ans, votre expertise en node.js, elle vaudra quoi ? Votre certification SCRUM ? Votre expertise Azure ?  

On le voit déjà, tous les développeuses et développeurs experts de techno dépassées (delphi, pascal, lisp…) galèrent à trouver du travail passé 50 ans, alors que dans le même temps les jeunes peinent à entrer sur le marché du travail.  

Ajoutons à cela que [l’espérance de vie en bonne santé est de 63 ans](https://drees.solidarites-sante.gouv.fr/publications/etudes-et-resultats/les-francais-vivent-plus-longtemps-mais-leur-esperance-de-vie-en) et ne progresse pas. Autrement dit, nous partirons tous à la retraite [diminués ou malades](https://www.francetvinfo.fr/economie/retraite/reforme-des-retraites/reforme-des-retraites-62-ans-c-est-deja-a-la-limite-du-supportable-estiment-ces-cadres-qui-veulent-que-l-on-reconnaisse-la-penibilite-psychique-du-travail_5518158.html). À quoi bon vivre une telle retraite ?

Disons le clairement, **Macron nous entraîne à la boucherie**.

Dans les faits les [économistes](https://www.alternatives-economiques.fr/cinq-pistes-combler-deficit-retraites-se-fatiguer/00105338), [les syndicats](https://solidaires.org/sinformer-et-agir/brochures/argumentaires/non-notre-systeme-de-retraites-nest-pas-en-peril/) et l'[État](https://www.assemblee-nationale.fr/dyn/16/textes/l16b0273_projet-loi.pdf) sont clairs:  

**➡ IL N'Y A PAS DE PROBLÈME POUR FINANCER NOTRE SYSTÈME DE RETRAITE ! ⬅**

Voici quelques chiffres pour illustrer notre propos :  
- En 2021, notre système de pensions a même été excédentaire de 900 millions d’euros !  
- Cette dynamique se prolonge en 2022 et connaît un excédent de 3,2 milliards d’euros.  
- Les dépenses retraites resteront inférieures à 14 % du PIB jusqu’en 2027  
- Elles sont appelées à baisser mécaniquement à partir de 2050 à cause de la sortie progressive de la génération des baby-boomers du système  
- Le déficit projeté serait au maximum de 0,8% du produit intérieur brut (PIB) en 2070 - soit entre 30 et 40 milliards d'euros  
- Un “*trou*” bien éloigné des 300 milliards du “*quoi qu’il en coûte*” trouvés pour passer la crise du Covid  


Autre problématique, si la réforme voit le jour, ce seront [les femmes](https://france.attac.org/se-mobiliser/reforme-des-retraites/article/retraites-une-reforme-plus-juste-pour-les-femmes-vraiment), encore une fois qui trinqueront.  


Donc oui, le gouvernement et les médias de masse disent n'importe quoi sur la question des retraites. Mais là, ça devient beaucoup trop voyant. Même les [syndicats très pro-grouvernement comme l'UNSA, la CFTC et la CFDT](https://solidaires.org/sinformer-et-agir/actualites-et-mobilisations/communiques/communique-intersyndical-retraites-pret-es-a-la-mobilisation/) le disent !  


Donc voilà, la question ne sera plus trop de savoir quand on fera grève, mais combien de personnes vous pourrez convaincre à la machine à café !  

En savoir plus sur la réforme des retraites: 
- [Retraites, le financement n'est pas un problème](https://solidaires.org/sinformer-et-agir/brochures/argumentaires/retraites-le-financement-nest-pas-un-probleme/)
- [Non, notre système de retraites n'est pas en péril](https://solidaires.org/sinformer-et-agir/brochures/argumentaires/non-notre-systeme-de-retraites-nest-pas-en-peril/)
- [Arguments contre le recul de l'âge de départ en retraite](https://solidaires.org/sinformer-et-agir/brochures/argumentaires/argument-pour-une-fiche-argumentaire-sur-le-recul-de-lage-de-depart-en-retraite/)
- [Pour le droit a une retraite digne et heureuse](https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/)