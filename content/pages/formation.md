---
title: "Formation : On vous explique tout !"
date: "{{ .Date }}"
draft: false
url: "/formation"
---

Vous trouverez ici des trucs et astuces pour vous former:  

### Pour OCTO :
- [Élection CSE 2023 @ OCTO : CSE, ASC, DS, CE... ON VOUS EXPLIQUE TOUT !](/pdf/slides_cse.pdf)  

### Droit du travail :
- [Fiches thématique pour connaitre ses droits](https://solidaires.org/connaitre-ses-droits/fiche-droits/)  (Formation continue, congés, arrêt maladie, etc)

### À propos de la grève :
- [Comment faire grève ?](https://greve.cool/)
- [Est-ce que c'est la grève ?](https://twitter.com/cestlagreve)

### À propos du syndicalisme (vidéo youtube)
- [Une histoire du syndicalisme (par Franck Lepage ~2h)](https://www.youtube.com/watch?v=W9K4nWRvBq8)
- [Qu'est ce que le sydnicalisme ? (~15 min)](https://www.youtube.com/watch?v=utlxTUO4eP8&t=3s)
- [Qu'est-ce que le syndicalisme révolutionnaire (~5 min)](https://www.youtube.com/watch?v=M2xYyQXdRS8)
- [Histoire du Syndicalisme Francais 1/2 (~8 min)](https://www.youtube.com/watch?v=L1putYUn1As)
- [Histoire du Syndicalisme Francais 2/2 (~15 min)](https://www.youtube.com/watch?v=yRfD2wPMxhk)
- [Qu'est-ce que la charte d'amien ? (~3 min)](https://www.youtube.com/watch?v=DmL2CQUNx1U)
- [L'autogestion (~3 min)](https://www.youtube.com/watch?v=Vp1IOHTsqvg)

