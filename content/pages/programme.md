---
title: "Notre Programme pour les élections 2023"
date: "{{ .Date }}"
draft: false
url: "/programme"
---

## Sommaire
1. [Nos priorités](#priorites)
2. [Représentation des Octos](#representation)  
   - [Comité d'Entreprise](#ce)  
   - [Fonctionnement du CSE et la bonne défense des Octos](#cse)  
   - [Campements](#camps)  
3. [Conditions de travail](#conditions)  
   - [Salaires](#salaires)  
   - [Fonction Coeur](#coeur)
   - [Management](#management)  
   - [Télétravail](#tt)  
   - [Outillage](#bruler-le-mdm)  
   - [Mobilités](#velo)  
4. [Protection des Octos](#protect)  
   - [Lutte contre les discriminations (Sexisme, Homophobie, Racisme, Handicaps, etc)](#woke)  
   - [Sécurité au travail](#secu)  
   - [Bien-être et santé au travail](#bien-etre)  

> Ce programme a été écrit collaborativement, entre les personnes qui figurent sur la  
> liste soutenue par Solidaires pour les élections de 2023. Si vous voulez y contribuer  
> n'hésitez pas à [nous contacter](/contact/).  


<br/>
<br/>

## 1- Nos priorités {#priorites}
Afin d'améliorer l'impact du CSE et de rendre possibles les propositions que nous défendons, nous avons décidé de nous concentrer au départ sur 4 axes principaux :
1. **Professionnaliser le CE** : pour proposer à tous les Octos et leurs familles une offre sociale et culturelle adaptée à la législation et aux nouvelles contraintes de vie (télétravail, hybride, etc).
2. **Promouvoir une culture de la transparence** concernant les discussions avec la direction en rendant compte des décisions prises.
3. **Créer et mettre à jour régulièrement la Base de Données Économiques, Sociales et Environnementales** (BDESE), pour permettre aux représentant•e•s des salarié•e•s l’accès aux informations à jour et pertinentes pour une meilleure défense des intérêts des Octos !
4. **Être au service des Octos** : Mettre en place une équipe disponible et formée pour aider à remonter les douleurs des Octos de manière efficace et actionnable. Cela commencera par l’envoi d’une enquête à toutes et tous afin de savoir ce que nous attendons du CSE.

<br/>

## 2- Représentation des Octos {#representation}

### 💵 Comité d'Entreprise {#ce}

> Il faudrait parler d'Activités Sociales et Culturelles (ASC) du <abbr title="Comité Social et Économique">CSE</abbr> plutôt que Comité d'Entreprise (CE) qui est le terme ancien. 

#### Les constats
* OCTO ne respecte pas l’[article L2312-81 du Code du Travail](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000036761976) à propos du budget qui doit être a minima constant vis-à-vis de la masse salariale, or, le budget a disparu depuis 2019.  
* Actuellement les membres du <abbr title="Comité Social et Économique">CSE</abbr> (élu.e.s solidaires compris) n'ont pas le temps, les compétences ni la motivation pour animer le <abbr title="Comité d'Entreprise">CE</abbr>
* Beaucoup d'ancien de BeNext regrettent la plateforme HelloCSE  
* Les budgets “*fun*” des ateliers, leagues, campements ou plénières sont portés par le management et la direction, ils ne doivent pas être confondus avec l’activité sociale et culturelle portée par les représentants des salariés.  
#### Nos Propositions
* Respecter la loi en termes de budget <abbr title="Activités Sociales et Culturelles">ASC</abbr> (feu <abbr>CE</abbr>) qui se reporte d'une année sur l'autre  
* **Négocier un vrai budget <abbr>ASC</abbr>** équivalent en dotation à celui d'<abbr title="Accenture">ACN</abbr> (Soit environ 0,6% de la masse salariale)  
* Embauche d'un·e salarié·e pour la gestion administrative, financière et événementielle du <abbr>CE</abbr>  
* Remettre en place une plateforme d'offre de sorties (Type HelloCSE)  
* Faire une liste des avantages qu'Accenture offre à ses salarié·e·s et dont nous pourrions bénéficier

### 📚 Fonctionnement du CSE et la bonne défense des Octos {#cse}

#### Les constats
* Il faut sortir de la culture du secret du <abbr>CSE</abbr> : c’est un lieu de communication entre employé·e·s et direction. Hors secrets de fabrication et sujets qui touchent des points personnels, tout doit être communicable  
* **L’absence de <abbr>BDESE</abbr>** (Base de Données Économiques, Sociales et Environnementales) chez OCTO.  
* Il y a un manque de professionnalisme dans le <abbr>CSE</abbr>, notamment dû au manque de formation légale des élu.e.s
* Le CSE manque d'un lieu de permanence et d'échange avec les Octos

#### Nos propositions
* Revenir à l'esprit de transparence chez OCTO dans le <abbr>CSE</abbr> et vis-à- vis des Octos.  
* Avoir des traces écrites qui reflètent mieux les échangent que les actuels compte rendus des réunions du <abbr>CSE</abbr>  
* **Mettre en place la <abbr title="Base de Données Économiques, Sociales et Environnementales">BDESE</abbr>**, indispensable pour que le <abbr>CSE</abbr> puisse faire son travail de représentation des Octos  
* Former les membres du <abbr>CSE</abbr> aux questions légales et sociales  
* Mettre à disposition du <abbr>CSE</abbr> un local.

### 🏕️ Campements {#camps}

#### Les constats
* **Les campements ne sont pas considérés comme des établissements aux yeux de la loi.** C’est une situation, légale mais un peu cavalière 🐎, qui permet d'éviter de s’encombrer de règles légales notamment liées à la fermeture d’établissements et à la représentation du personnel.  
* La différence de salaire entre les Octos “Paris” partis ailleurs en France et les Octos en campements est un irritant de plus en plus fort  

#### Nos Propositions
* Créer un établissement par campement, afin de mieux défendre les droits des Octos qui y sont rattachés
* Faire une grande négociation des salaires sur les campements en comparant les nuages de points des salarié.e.s en campements et ceux rattaché·e·s à Paris mais installé•e•s ailleurs en France  
* Garantir le remboursement des voyages en train et des hôtels pour les salarié·e·s issus des campements et contraints de se déplacer en Ile-de-France. Il s'agit de déplacements professionnels et doivent être considérés comme tels
* Introduire des Délégués Syndicaux (<abbr title="Délégués Syndicaux">DS</abbr>) et référents de proximité dans chaque campement..  
* Appliquer la loi. **À travail égal, salaire égal.**  

<br/>

## 3- Conditions de travail {#conditions}

### 💰 Salaires {#salaires}

#### Les constats
* Le taux de turn over a baissé durant la période COVID. Nous sommes à environ 17% de salarié·e·s qui quittent OCTO tous les ans  
* Beaucoup de personnes ressentent que les salaires OCTO sont plus bas que ceux du marché.  
* La <abbr>BDESE</abbr> obligatoire, n’existe pas. Les représentants des salariés n’ont pas les informations nécessaires à leurs actions.  

#### Nos Propositions
* Mettre enfin en place la <abbr>BDESE</abbr>, mise à jour a minima tous les trimestres  
* Faire appel à un cabinet d'expertise pour avoir un vrai benchmark des salaires par rapport au marché  
* S'assurer que l'augmentation de la masse salariale annuelle corresponde à l'augmentation du coût de la vie
* Lors de la phase des EEO, s'assurer que les Octos arrivés depuis moins de 6 mois puissent bénéficier d'un boost si leur salaire est en décalage avec le marché
* **Garantir au minimum une indexation des salaires sur l’inflation**, il n'est pas acceptable que notre pouvoir d'achat baisse  
* Les normes de <abbr title="Taux d'Activité Congés Exclus">TACE</abbr> attendues des Octos devraient être indexées sur le niveau des ventes (ce n’est pas de la responsabilité des consultant•e•s si le niveau des ventes n’est pas le bon). L'évaluation des objectifs de <abbr>TACE<abbr> lors des <abbr title="Entretiens annuels d'Évaluations et d’Orientations">EEO</abbr> doit être revue à la baisse proportionnellement à la non-atteinte des objectifs de vente.

### 💗 Fonction Coeur {#coeur}
#### Les constats
* Les fonctions coeurs sont encore trop vues comme des centres de coût et non comme une force pour OCTO
* Le nombre de points de bonus des non-consultants n'évolue pas en fonction de l'ancienneté
* Il manque une culture propre aux fonctions coeur pour avoir plus de synergie
* Les possibilités d'évolution de carrière au sein des fonctions coeur ne sont pas assez définies

#### Nos propositions
* Donner automatiquement un point de bonus aux non-consultants tous les 5 ans
* Créer une journée culture *fonction coeur* sur le modèle des journées tribus
* Avoir du temps dédié pour pouvoir travailler sur des sujets transverses
* Avoir un vrai *framework carrière* pour les fonctions coeurs

### 🐙 Management {#management}

#### Les constats
* La réorganisation qui vient d’en haut. Quid de la **sociocratie** chez OCTO ?  
* Un désengagement croissant des Octos de la vie de l’entreprise (participations aux <abbr title="Birds of a Feather">BoF</abbr>, <abbr title="Brown Bag Lunch">BBL</abbr>, cercles, formations, entretiens de recrutement, etc.).  
#### Nos Propositions
* Nous voulons avancer vers une société qui soit vraiment **agile** et qui respecte vraiment la sociocratie  
* Nous souhaitons viser l'auto-gestion dans les ateliers  

### 🛩️ Télétravail {#tt}

#### Les constats {#tt-analyse}
* La dernière enquête sur le télétravail chez OCTO nous a appris que : <sup>[1](#source-anywhere)</sup>
   * 15% des Octos vivent en dehors d'Île-de-France
   * 38% des Octos ont plus d'une heure de trajet pour venir travailler
   * ~60% des Octos ne souhaitent pas venir chez le client plus de 2 jours par semaine
* Les indemnités d'installation et les remboursements de <abbr title="TéléTravail">TT</abbr> sont trop faibles  
* Augmentation du prix de l'énergie (bouclier tarifaire à 15% en 2023)  
* Le <abbr>TT</abbr> a permis à OCTO de ne pas augmenter la surface de locaux, ce qui a réduit les coûts fixes par personne
* L’expérimentation 60% de télétravail garantie a été terminée, en invoquant principalement la raison qu’il y a eu peu de demandes. Nous considérons que la communication à ce sujet a été plutôt légère, et que la pression du présentiel de plus en plus forte chez le client a limité les demandes liées à cette expérimentation.  

#### Nos Propositions
* **Renégocier à la hausse les primes liées à l'accord télétravail**, principalement liées à la participation aux dépenses d'énergie qui ont beaucoup augmenté. Les indexer aux évolutions du coût de l'énergie. 
* Augmenter les primes d'installation.
* Relancer l’expérimentation 60% <abbr>TT</abbr> garanti en renforçant la communication  
* Trouver un partenariat avec un ou plusieurs hôtels, pour proposer 5 nuits / mois aux Octos non parisiens à des tarifs raisonnables pour OCTO

### 🛠️ Outillage {#bruler-le-mdm}

> L’employeur est tenu d’assurer aux salariés les moyens d’assurer leurs missions. Or, certains outils Accenture nous handicapent plus qu’ils ne nous aident.  

#### Les constats 
* Globalement, les outils issus du monde Accenture sont de mauvaise qualité (<abbr title="Mobile Device Management">MDM</abbr>, Pare Feu, MyLearning, logiciel de gestion mailing liste, bricolage autour de boîtes mails, redémarrages impromptus …)  
* Le <abbr>MDM</abbr> aggrave l’obsolescence programmée de nos équipements informatiques, et joue sérieusement contre les objectifs de décarbonation d’OCTO  
* Le filtrage des réseaux nous oblige régulièrement à contourner le réseau de l’entreprise (on passe tous par la 4G quand il faut se connecter à un <abbr title="Virtual Private Network">VPN</abbr>, par exemple) 

#### Nos propositions
* Faire un feu de joie avec le MDM  
* Renforcer les moyens sur nos outils internes pour s’affranchir des outils ACN  
* Garantir la [neutralité des réseaux](https://fr.wikipedia.org/wiki/Neutralit%C3%A9_du_r%C3%A9seau) chez OCTO, pour que nous puissions travailler dans de bonnes conditions  
* Monter un club d’entre-aide pour réparer, configurer, installer nos ordinateurs et téléphones en cohérence avec l’éco-système vertueux de la culture du libre et les objectifs de [Make IT Last](https://octo.atlassian.net/wiki/spaces/SO/pages/1379270663/La+DSI+s+engage+aussi).

### 🚲 Mobilités {#velo}

#### Les constats
* L'Indemnité Kilométrique Vélo (<abbr>IKV</abbr>) a été de nouveau repoussée aux calendes grecques  
* Nous ne sommes pas intéressés par les modèles d’Accenture qui proposent une carte de location de vélo  
* Le transport des salarié·e·s est l’un des leviers majeurs de décarbonation pour OCTO et ne pas avoir une action forte sur les mobilités douces ne nous aide pas à atteindre les objectifs qu’OCTO s’est fixé  
* Les transports en commun participent aussi à cet objectif.  

#### Nos Propositions
* **Mettre en place les propositions de la loi [<abbr>LOM</abbr> (Loi D'orientation Mobilités)](https://www.ecologie.gouv.fr/loi-dorientation-des-mobilites)** qui comprend entre autre l’Indemnité Kilométrique Vélo (<abbr>IKV</abbr>) (jusqu'à 200€ par an sans impôts)
* Obtenir un remboursement à 100% des abonnements de type Pass Navigo, à la fois pour des raisons écologiques et de préservation du pouvoir d’achat des Octos.  
* Prise en compte du temps de trajet domicile-travail comme du temps de travail pour les octos en contrat horaires, en particulier pour les longs trajets (supérieurs à 1 heure)  
* Achat d’une petite flotte de vélos d'entreprise à mettre à disposition des salarié·e·s  

<br/>

## 4- Protection des Octos {#protect}

### 🌈 Lutte contre les discriminations (Sexisme, Homophobie, Racisme, Handicaps, etc) {#woke}

#### Les constats

* Bon nombre d'Octos nous remontent des situations de discrimination ordinaire  
* Une bonne partie des personnes avec qui nous avons discuté du sujet nous dit avoir été déçue de la prise en compte de leur situation lorsqu'ils l'ont remontée.  
* 100% des femmes de retour de congés mat' ont eu des augmentations dans la moyenne et/ou une promotion l'année suivant leur grossesse  
* L’absence de <abbr>BDESE</abbr> empêche le <abbr>CSE</abbr> de faire son travail de veille sur ces sujets. :  
   * évolution de l’égalité professionnelle et salariale  
   * progression professionnelle et salariale des femmes après une grossesse  
   * grade/salaire d'entrée des femmes par rapport aux hommes d'âge et d'expérience comparables  

#### Nos Propositions
* Nous souhaitons inscrire systématiquement le sujet des discriminations à l’ordre du jour du <abbr>CSE</abbr>  
* Rendre obligatoires les formations anti-discrimination pour tout le monde  
* Mieux communiquer sur les procédures/outils de remontée d'alertes  
* Lancer une étude par un cabinet d'expert pour mesurer tous les sujets de discrimination chez OCTO  
* Nous nous engageons à utiliser systématiquement notre droit d’alerte sur les cas de discrimination  
* Mesurer les différences salariales hommes-femmes à niveau équivalent et les rendre disponible à toustes


### ⛑️ Sécurité au travail {#secu}

> Ce point aborde les problématiques de sécurité au travail, particulièrement de l’organisation des secours en cas d’incident (malaise, incendie…) demandant une intervention urgente et immédiate.  

#### Les constats
* Les agents de sécurité ne sont plus assez nombreux compte-tenu de la masse salariale. Si un malaise survient, ils ne peuvent pas être partout en même temps.  
* La signalétique (notamment le numéro d’appel des agents) n’est pas assez visible  
* Il y a un seul défibrillateur pour tout le 34 là où il en faudrait un par étage  
#### Nos propositions
* Doubler les effectifs des agents de sécurité en les passant de 2 à 4  
* Équiper tous les étages en défibrillateurs  
* Rendre plus visibles les Octos formés aux premiers secours au travail  
* Améliorer la signalétique (notamment le numéro d’appel des agents)

### 🧘‍♀️Bien-être et santé au travail {#bien-etre}

> Ce point aborde les questions de bien-être et de santé au travail, entre autres s’agissant de la prévention et de la prise en charge des risques psychosociaux ou des TMS et autres maladies professionnelles.  

#### Les constats 
* Les risques psychosociaux peuvent être amplifiés par le télétravail (isolement, difficulté à remonter les alertes et à sensibiliser les salariés…), en particulier pour les femmes (plus susceptibles d’être victimes de violences intra-familiales ou de rencontrer des difficultés à séparer vie privée et professionnelle)  
* Le télétravail est aussi un facteur d'inégalité entre salariés face aux conditions matérielles de son exercice (taille du logement, présence de tiers, adaptation ou non de l’espace de travail, qualité de la connexion…)  
* L’ensemble des locaux ne dispose pas de postes de travail ergonomiques, en particulier au Vaisseau  

#### Nos propositions
* Procéder à des évaluations régulières des risques psychosociaux dans l’entreprise par un cabinet d'experts sur le sujet  
* Financer les équipements d’adaptation du poste de travail tels que les sièges ergonomiques  
* Relancer les formations "gestes & postures" et les appels aux formations par des orthoptistes  


<p id="source-anywhere"><small>1. Données provenants des réponses au questionnaire OCTO From Anywhere de mars 2022. <a href="#tt">Retour</a></small></p>
