---
title: "Le Syndicat Solidaires"
date: "{{ .Date }}"
draft: false
url: "/syndicat"
---

Nous sommes des salarié·e·s d'OCTO, et présentons une liste aux élections professionnelles 2023. Cette liste est soutenue par Solidaires Informatique. Mais qu'est-ce que le syndicat Solidaires, et quelles sont ses valeurs ?

## Un syndicalisme de lutte dans une <abbr title="Entreprise de Services Numériques">ESN</abbr> de luxe (!)

OCTO est, globalement, une bonne entreprise. Comme dans tous les groupes humains, il y a des problèmes, des points à corriger et des améliorations à faire. Bref, _There Is A Better Way_. Nous sommes là pour améliorer les choses, jusqu'à ce que le monde entier soit parfait !  

### SOLIDAIRES : Un syndicalisme auto-gestionnaire et révolutionnaire

[SOLIDAIRES](https://solidaires.org/), appelé aussi [_SUD_](https://fr.wikipedia.org/wiki/Union_syndicale_Solidaires) est un regroupement de syndicats de plusieurs branche (Sud-éducation, SOLIDAIRES Justice, Sud-Rail, SOLIDAIRES étudiant·e·s ...)  

Nous faisons partie de [SOLIDAIRES-Informatique](https://solidairesinformatique.org/), branche qui regroupes les salarié·e·s du conseil, du jeux vidéo et du numérique plus globalement.  

SOLIDAIRES est un syndicat anti-fasciste, féministe, [révolutionnaire](https://www.youtube.com/watch?v=M2xYyQXdRS8), [auto-gestionnaire](https://www.youtube.com/watch?v=Vp1IOHTsqvg), écologiste et anti-capitaliste.  

Plus globalement, SOLIDAIRES, s'inscrit dans la tradition du syndicalisme de gauche anti-autoritaire.  

### Vous êtes un syndicat politique alors ?

Oui, au sens où nous vivons dans une société qui fait des choix politiques en permanence et que nous voulons y prendre notre part.  
Quand le gouvernement fait une loi de réforme des retraites, cela nous concerne directement en tant que travailleuse ou travailleur.  

Quand un autre gouvernement fait des lois sur la chasse ou sur les OGM, là aussi cela concerne nos vies et nous pensons que notre rôle de syndicat est de prendre position.  

### Vous soutenez des candidats politiques ?

Non, nous sommes attachés à la [Charte d'Amiens](https://www.youtube.com/watch?v=DmL2CQUNx1U&t=23s), un vieux texte qui dit que le rôle du syndicalisme est de faire de la politique, mais de ne pas se préocuper _"des partis et des sectes"_ .  
Nous sommes donc apartisan (ne soutenant pas un parti) et sans religion.  
Nous n'appellerons donc jamais à voter pour quelqu'un.  

À coté de cela, nous sommes *anti-fasciste*. C'est-à-dire que nous combattons clairement l'extrême droite et ses idées, partout où elles se trouvent. On ne peut pas être adhérent à SOLIDAIRES et proférer des thèses racistes, islamophobes, antisémites etc.. ou une appartenance aux partis d'extrême droite.  

### Nos différences avec les autres syndicats ?

Il existe une large palette de [syndicalismes](https://www.youtube.com/watch?v=utlxTUO4eP8) en France.  
On peut les regrouper en 3 axes:  
- Un axe "autoritaires" vs "anti-autoritaire"  
- Un axe "dialogue" vs "lutte"  
- Un axe "corporation" vs "politique"  

#### Nous sommes un syndicalisme anti-autoritaire:
Personnes en "*haut*" du syndicat (national, départemental...) ne valide ou nous dicte ce qu'on doit faire dans notre section syndicale.  
Si la liberté est totale, il faut reconnaitre que l'organisation est parfois un peu bordélique !  

#### Nous sommes un syndicalisme de lutte des classes:
Dans notre système capitaliste, le but d'une entreprise est de faire de l'argent.  
Sans méchanceté contre notre direction, nous savons qu'elle veut être en croissance et gagner le plus d'argent possible.  

Dans le même temps, nous pensons que les intérets des [salarié·e·s](https://www.youtube.com/watch?v=lH1g4q1MoXE) sont aussi de gagner le plus d'argent possible tout en travaillant le moins possible.  
Nos intérêts sont donc antagonistes.  

Certains syndicats pensent qu'en dialoguant beaucoup, on peut trouver des intérêts communs, que la négociation peut permettre de rendre tout le monde heureux.  

Nous observons que, dans le temps, ce type de méthode syndicale perd plus de choses qu'elle n'en gagne.  

Nous ne sommes pas contre la négociation par principe, mais nous savons que nous sommes plus utile aux salairié·e·s en installant un rapport de forces avec la direction.

#### Nous sommes un syndicat politisé qui défend les salarié·e·s:

Un certains syndicalisme se dit *corporatiste*. C'est-à-dire qu'il ne défend que les intérêts de sa branche et uniquement de sa branche. Sans se soucier du sort des autres travailleurs et travailleuses des autres secteurs, voire des autres entreprises (appelé aussi _Syndicat-Maison_).  

Nous observons que ces syndicats soutiennent, en général, les intérêts de la direction.  

À cela, nous préférons un syndicalisme "_de classes_" qui défend tous les salarié·e·s.  

Par ailleurs, le droit du travail étant national, nous préferons être dans une structure qui permet des rencontres et des formations communes entre toutes les branches. Nos combats dans la rue en sont aussi plus forts.
<!--  
## Notre lecture du _Why_ d'OCTO

**Version FY21**  

_Nous croyons que l’informatique transforme nos sociétés. Nous savons que les réalisations marquantes sont le fruit du partage des savoirs et du plaisir à travailler ensemble. Nous recherchons en permanence des meilleures façons de faire._  

Nous lisons ce Why avec attention: _L'informatique transforme nos sociétéS_ : Les entreprises bien-sûr, mais aussi *LA* société de manière générale. L'entreprise n'est pas hors du monde, elle en fait partie prenante. Nore rôle de syndiqué est d'être là dans cette transformation pour les salarié·e·s bien sûr, mais aussi pour tout les habitants de la planète.  

_Les réalisations marquantes sont le fruit du partage des savoirs et du plaisir à travailler ensemble_ : Oui, nous croyons au partage des savoirs. C'est pourquoi notre syndicat, Solidaires Informatique à toujours défendu le logiciel libre. Nous pensons que les brevets et, de manière générale, la propriété privé des moyens de productions, bloque toujours ce partage des connaissances.  

Quand au _plaisir à travailler ensemble_, y a t'il besoin d'expliquer qu'un syndicat se bat pour cela ? -->  
