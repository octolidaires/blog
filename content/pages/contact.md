---
title: "Nous contacter"
date: "{{ .Date }}"
draft: false
url: "/contact"
---

Pour plus d'informations autour de la liste soutenue par Solidaires, tu peux contacter les personnes suivantes : 

- TLU
- SEBA
- LIBO
- EST
- JORO
- YART